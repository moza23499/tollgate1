package com.student.model.dao;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
public class StudentDaoImpl implements StudentDao{
	public static void main(String[] args) {
		
		Properties properties=new Properties();
		String url=null,
		username=null,
		password=null,
		driverName=null;
		
		try {
			InputStream is=new FileInputStream("db.properties");
			properties.load(is);
			
			
			driverName=properties.getProperty("jdbc.driverName");
			url=properties.getProperty("jdbc.url");
			username=properties.getProperty("jdbc.username");
			password=properties.getProperty("jdbc.password");
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			
			
			Class.forName(driverName);
			System.out.println("driver is loaded");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		try (Connection connection = DriverManager.getConnection(url,
				username, password)) {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("select * from student");
			while (rs.next()) {
				System.out.println(rs.getInt("id") + ": " + rs.getString("fname") + ": " + rs.getString("subjectname") + ": " + rs.getInt("subjectmarks")
						+ ": "+ rs.getInt("testnumber"));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}	

	@Override
	public List<Student> getAllStudents() {
		
		return null;
	}


}
