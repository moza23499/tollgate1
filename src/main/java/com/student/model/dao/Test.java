package com.student.model.dao;

import java.util.*;
import java.util.Map.Entry;

public class Test {
	public static void main(String[] args) {
		StudentDao sdao = new StudentDaoImpl();
		
		List<Student> studentList = sdao.getAllStudents();	
		Map<Integer,String> smap = new HashMap<Integer,String>();
		smap.put(62, "sushant");
		smap.put(54, "neeraj");
		smap.put(87, "poojitha");
		smap.put(76, "syed");
		smap.put(62, "abdul");
		Set<Entry<Integer,String>> es = smap.entrySet();
		System.out.println("results as per marks");
		es.stream().sorted(Map.Entry.comparingByKey()).forEach(System.out::println);
		
		
		System.out.println("results as per names");
		es.stream().sorted(Map.Entry.comparingByValue()).forEach(System.out::println);
		}
}
