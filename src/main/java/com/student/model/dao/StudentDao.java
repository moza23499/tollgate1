package com.student.model.dao;

import java.util.List;

public interface StudentDao
	{
	List<Student> getAllStudents();
	}
